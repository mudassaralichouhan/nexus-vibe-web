Nexus Vibe
=

![](./public/icon.jpg)

## _The social network that resonates._


[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://github.com/mudassaralichouhan/nexus-vibe.git)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](http://github.com/mudassaralichouhan/nexus-vibe.git)

User-generated Content (UGC), Trending, Hashtags Influences, Community, Content, Engagement

- Multimedia posts
- Viral content
- ✨Magic ✨

### Backend dependence [api-server](http://github.com/mudassaralichouhan/nexus-vibe)

    ~# git clone github.com/mudassaralichouhan/nexus-vibe.git

### Installation
    ~# git clone https://gitlab.com/mudassaralichouhan/nexus-vibe-web.git

    ~# npm install
    ~# npm run dev

* User
> http://localhost:5173

* Admin
> http://localhost:5173/admin/dashboard


## License

MIT

**Free Software, Hell Yeah!**