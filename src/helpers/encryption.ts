import CryptoJS from "crypto-js";


const secretKey: string = import.meta.env?.VITE_SECRET_KEY || '49anGg1ywRQFxx3bZCxmVgVnrTwMxYMO';

export const encryptData = (data: object) => {
  return CryptoJS.AES.encrypt(JSON.stringify(data), secretKey).toString();
};

export const decryptData = (ciphertext: string): object | null => {
  try {
    const bytes: CryptoJS.lib.WordArray = CryptoJS.AES.decrypt(ciphertext, secretKey);
    return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  } catch (error) {
    return null;
  }
};