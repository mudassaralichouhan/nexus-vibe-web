import axios from 'axios';

const baseurl = import.meta.env.VITE_API_URL;

export interface ResponseType {
  data: any;
  error: boolean;
}

const apiClient = axios.create({
  baseURL: baseurl,
  headers: {
    'Content-type': 'application/json'
  }
});

apiClient.interceptors.request.use((req) => {
  if (localStorage.getItem('auth')) req.headers.Authorization = JSON.parse(localStorage.getItem('auth') || '').token;
  return req;
});

apiClient.interceptors.response.use(response => {
  return response;
}, async error => {

  if (error.response.status === 401) {

  }

  return Promise.reject(error);
});


export const api = axios.create({
  baseURL: baseurl,
  headers: {
    'Content-type': 'application/json'
  }
});

export default apiClient;
