import axios from 'axios';
import authService from "@/services/auth-service.ts";

const baseurl = import.meta.env.VITE_ADMIN_API_URL;

const apiClient = axios.create({
  baseURL: baseurl,
  headers: {
    'Content-type': 'application/json'
  }
});

apiClient.interceptors.request.use((req) => {
  const auth = authService.get()
  console.log(auth);
  if (auth)
    req.headers.Authorization = 'Bearer ' + auth.token;
  return req;
});

apiClient.interceptors.response.use(response => {
  return response;
}, async error => {

  console.log('refresh token');
  const auth = authService.get()

  if (error.response?.status === 401) {
    console.log(auth)
  }

  if (error.response?.status === 403) {
    try {
      const response = await api.post('/auth/token/refresh', {
        refreshToken: auth?.refresh_token || ''
      })

      authService.set({
        ...auth,
        token: response.data?.accessToken,
      });
    } catch (error) {

      if (error.response.status === 401) {
        authService.set({
          isAuthenticated: false,
          token: null,
          refresh_token: null,
          expireAt: 0,
          data: null
        });
      }

    }
  }

  return Promise.reject(error);
});


export const api = axios.create({
  baseURL: baseurl,
  headers: {
    'Content-type': 'application/json'
  }
});

export default apiClient;
