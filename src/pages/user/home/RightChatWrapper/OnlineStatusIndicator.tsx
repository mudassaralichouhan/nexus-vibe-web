import React from "react";
import {humanReadable} from "@/utils/dayjs.ts";

interface OnlineStatusIndicatorProps {
  item: {
    active: boolean;
    timestamp: string;
  };
}

const OnlineStatusIndicator: React.FC<OnlineStatusIndicatorProps> = ({ item }) => {
  if (item?.active) {
    return <span className="bg-success ms-auto btn-round-xss"></span>;
  }
  
  if (!item?.active) {
    if (item?.timestamp) {
      const timeDifference = humanReadable(item.timestamp);
      
      if (timeDifference.includes("seconds")) {
        return <span className="bg-warning ms-auto btn-round-xss"></span>;
      } else {
        return (
          <span className="badge mt-0 text-grey-500 badge-pill pe-0 font-xsssss">
            {timeDifference}
          </span>
        );
      }
    } else {
      return <span className="bg-warning ms-auto btn-round-xss"></span>;
    }
  }
  
  return null;
};


export default OnlineStatusIndicator;