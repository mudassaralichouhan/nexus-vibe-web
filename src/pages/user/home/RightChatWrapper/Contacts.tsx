import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";

import OnlineStatusIndicator from "./OnlineStatusIndicator.tsx";

const fetchContactsData = async () => {
  const date = new Date();
  await new Promise(resolve => setTimeout(resolve, 1000));
  return [
    {
      name: "Hurin Seary",
      image: '/assets/images/user-8.png',
      count: 2,
      timestamp: date.toLocaleString(),
      active: true
    },
    {
      name: "Victor Exrixon",
      image: '/assets/images/user-7.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: true
    },
    {
      name: "Surfiya Zakir",
      image: '/assets/images/user-6.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: false
    },
    {
      name: "Goria Coast",
      image: '/assets/images/user-5.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: true
    },
    {
      name: "Hurin Seary",
      image: '/assets/images/user-4.png',
      count: null,
      timestamp: date.setDate(date.getDate() - 7).toLocaleString(),
      active: false
    },
    {
      name: "David Goria",
      image: '/assets/images/user-3.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: false
    },
    {
      name: "Seary Victor",
      image: '/assets/images/user-2.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: true
    },
    {
      name: "Ana Seary",
      image: '/assets/images/user-12.png',
      count: null,
      timestamp: date.toLocaleString(),
      active: true
    },
  ];
}

const Contacts: React.FC = () => {
  const [list, setList] = useState([]);
  
  useEffect(() => {
    (async () => {
      const data = await fetchContactsData();
      setList(data);
    })();
  }, []);
  
  return (
    <React.Fragment>
      {list.length > 0 ? (
        <div className="section full pe-3 ps-4 pt-4 position-relative">
            <h4 className="font-xsssss text-grey-500 text-uppercase fw-700 ls-3">CONTACTS</h4>
            <ul className="list-group list-group-flush">
              
              {list.map((item, index) => (
                <li
                  key={index}
                  className="bg-transparent list-group-item no-icon pe-0 ps-0 pt-2 pb-2 border-0 d-flex align-items-center"
                >
                  <figure className="avatar float-left mb-0 me-2">
                    <img src={item.image} alt="image" className="w35" />
                  </figure>
                  <h3 className="fw-700 mb-0 mt-0">
                    <Link className="font-xssss text-grey-600 d-block text-dark model-popup-chat" to="#">
                      {item.name}
                    </Link>
                  </h3>
                  
                  {item.count !== null && (
                    <span className="badge badge-primary text-white badge-pill fw-500 mt-0">
                      {item.count}
                    </span>
                  )}
                  <OnlineStatusIndicator item={item}/>
                </li>
              ))}
            
            </ul>
          </div>
      ) : (
        <div className="preloader-wrap p-3">
          <div className="box shimmer">
            <div className="lines">
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Contacts;
