import React from "react";

interface OnlineStatusIndicatorProps {
  item: {
    timestamp: string;
  };
}

const OnlineStatusIndicatorLoss: React.FC<OnlineStatusIndicatorProps> = ({ item }) => {
  const currentTime = new Date();
  const itemTime = new Date(item.timestamp);
  
  if ((currentTime.getTime() - itemTime.getTime()) < 3 * 60 * 1000)
    return <span className="bg-success ms-auto btn-round-xss"></span>;
  
  return null;
};

export default OnlineStatusIndicatorLoss;
