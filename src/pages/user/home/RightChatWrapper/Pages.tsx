import React, { useEffect, useState } from "react";
import OnlineStatusIndicatorLoss from "@/pages/user/home/RightChatWrapper/OnlineStatusIndicatorLoss.tsx";

const fetchPagesData = async () => {
  await new Promise((resolve) => setTimeout(resolve, 1100));
  return [
    {
      color: "bg-red-gradiant",
      title: "Studio Express",
      timestamp: "2 min",
    },
    {
      color: "bg-gold-gradiant",
      title: "Armany Design",
      timestamp: "",
    },
    {
      color: "bg-blue-gradiant",
      title: "De fabous",
      timestamp: "20 min",
    },
  ];
};

const Pages: React.FC = () => {
  const [list, setList] = useState([]);
  
  useEffect(() => {
    (async () => {
      const data = await fetchPagesData();
      setList(data);
    })();
  }, []);
  
  return (
    <React.Fragment>
      {list.length > 0 ? (
        <div className="section full pe-3 ps-4 pt-4 pb-4 position-relative">
          <h4 className="font-xsssss text-grey-500 text-uppercase fw-700 ls-3">
            PAGES
          </h4>
          <ul className="list-group list-group-flush">
            {list.map((item, index) => (
              <li
                key={index}
                className={`bg-transparent list-group-item no-icon pe-0 ps-0 pt-2 pb-2 border-0 d-flex align-items-center`}
              >
                <span
                  className={`btn-round-sm ${item.color} me-3 ls-3 text-white font-xssss fw-700`}
                >
                  {item.title
                    .split(" ")
                    .map((word) => word[0].toUpperCase())
                    .join("")}
                </span>
                <h3 className="fw-700 mb-0 mt-0">
                  <a
                    className="font-xssss text-grey-600 d-block text-dark model-popup-chat"
                    href="#"
                  >
                    {item.title}
                  </a>
                </h3>
                <OnlineStatusIndicatorLoss item={item} />
              </li>
            ))}
          </ul>
        </div>
      ) : (
        <div className="preloader-wrap p-3">
          <div className="box shimmer">
            <div className="lines">
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
              <div className="line s_shimmer"></div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Pages;
