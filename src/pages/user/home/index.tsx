import React from "react";

import Header from './header/Index.tsx';
import Navigation from './navigation/Index.tsx';
import RightChatWrapper from "@/pages/user/home/RightChatWrapper/RightChatWrapper.tsx";
import MainContent from "@/pages/user/home/MainContent/MainContent.tsx";

const Home: React.FC = () => {
  return (
    <>
      <Header />
      <Navigation />
      <MainContent />
      <RightChatWrapper />
    </>
  );
};

export default Home;
