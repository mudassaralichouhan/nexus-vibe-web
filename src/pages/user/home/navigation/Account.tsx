import React from "react";
import {Link} from "react-router-dom";

const list = [
  {
    to: "/settings",
    title: "Settings",
    count: 0,
  },
  {
    to: "/analytics",
    title: "Analytics",
    count: 0,
  },
  {
    to: "/chat",
    title: "Chat",
    count: 23,
  },
];

const Account: React.FC = () => {
  return (
    <React.Fragment>
      <div className="nav-wrap bg-white bg-transparent-card rounded-xxl shadow-xss pt-3 pb-1">
        <div className="nav-caption fw-600 font-xssss text-grey-500">
          <span></span>
          Account
        </div>
        
        <ul className="mb-1">
          <li className="logo d-none d-xl-block d-lg-block"></li>
          {list.map((item, index) => (
            <li key={index}>
              <Link to={item.to} className="nav-content-bttn open-font h-auto pt-2 pb-2">
                <i className="font-sm feather-message-square me-3 text-grey-500"></i>
                <span>{item.title}</span>
                {item.count > 0 && <span className="circle-count bg-warning mt-0">{item.count}</span>}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default Account;
