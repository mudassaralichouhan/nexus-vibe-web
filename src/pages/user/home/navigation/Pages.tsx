import React from "react";
import { Link } from "react-router-dom";

const list = [
  {
    to: "/mails",
    title: "Email Box",
    count: 584,
    icon: 'feather-inbox'
  },
  {
    to: "/hotel",
    title: "Near Hotel",
    count: 0,
    icon: 'feather-home'
  },
  {
    to: "/event",
    title: "Latest Event",
    count: 0,
    icon: 'feather-map-pin'
  },
  {
    to: "/live",
    title: "Live Stream",
    count: 0,
    icon: 'feather-youtube'
  },
];

const Pages: React.FC = () => {
  return (
    <React.Fragment>
      <div className="nav-wrap bg-white bg-transparent-card rounded-xxl shadow-xss pt-3 pb-1 mb-2">
        <div className="nav-caption fw-600 font-xssss text-grey-500"><span>More </span>Pages</div>
        <ul className="mb-3">
          {list.map((item, index) => (
            <li key={index}>
              <Link to={item.to} className="nav-content-bttn open-font">
                <i className={`font-xl text-current ${item.icon} me-3`}></i>
                <span>{item.title}</span>
                {item.count > 0 && <span className="circle-count bg-warning mt-1">{item.count}</span>}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default Pages;
