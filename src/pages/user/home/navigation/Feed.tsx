import React from "react";
import { Link } from "react-router-dom";

const list = [
  {
    to: "/",
    title: "Newsfeed",
    icon: {name: 'feather-tv', color: 'bg-blue-gradiant'}
  },
  {
    to: "/badges",
    title: "Badges",
    icon: {name: 'feather-award', color: 'bg-red-gradiant'}
  },
  {
    to: "/stories",
    title: "Explore Stories",
    icon: {name: 'feather-globe', color: 'bg-gold-gradiant'}
  },
  {
    to: "/groups",
    title: "Popular Groups",
    icon: {name: 'feather-zap', color: 'bg-mini-gradiant'}
  },
  {
    to: "/profile",
    title: "Author Profile",
    icon: {name: 'feather-user', color: 'bg-primary-gradiant'}
  },
];

const Feed: React.FC = () => {
  return (
    <React.Fragment>
      <div className="nav-wrap bg-white bg-transparent-card rounded-xxl shadow-xss pt-3 pb-1 mb-2 mt-2">
        <div className="nav-caption fw-600 font-xssss text-grey-500">
          <span>New </span>
          Feeds
        </div>
        <ul className="mb-1 top-content">
          <li className="logo d-none d-xl-block d-lg-block"></li>
          {list.map((item, index) => (
            <li key={index}>
              <Link to={item.to} className="nav-content-bttn open-font">
                <i className={`${item.icon.name} btn-round-md ${item.icon.color} me-3`}></i>
                <span>{item.title}</span>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default Feed;