import React from "react";

import Feed from "./Feed.tsx";
import Pages from "./Pages.tsx";
import Account from "./Account.tsx";

const Navigation: React.FC = () => {
  return (
    <nav className="navigation scroll-bar">
      <div className="container ps-0 pe-0">
        <div className="nav-content">
          <Feed />
          <Pages />
          <Account />
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
