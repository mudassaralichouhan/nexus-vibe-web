import React from "react";

import Notification from "./Notification.tsx";
import Setting from "./Setting";
import { Link } from "react-router-dom";
import {Box} from "@mui/material";

const Header: React.FC = () => {
  return (
    <Box className="nav-header bg-white shadow-xs border-0">
      
      <Box className="nav-top">
        <Link to={'/'}>
          <i className="feather-zap text-success display1-size me-2 ms-0"></i>
          <span className="d-inline-block fredoka-font ls-3 fw-600 text-current font-xxl logo-text mb-0">
              Social.
          </span>
        </Link>
        <Link to={'#'} className="mob-menu ms-auto me-2 chat-active-btn">
          <i className="feather-message-circle text-grey-900 font-sm btn-round-md bg-greylight"></i>
        </Link>
        <Link to={"/videos"} className="mob-menu me-2">
          <i className="feather-video text-grey-900 font-sm btn-round-md bg-greylight"></i>
        </Link>
        <Link to={"#"} className="me-2 menu-search-icon mob-menu">
          <i className="feather-search text-grey-900 font-sm btn-round-md bg-greylight"></i>
        </Link>
        <button className="nav-menu me-0 ms-2"></button>
      </Box>
      
      <form action="#" className="float-left header-search">
        <Box className="form-group mb-0 icon-input">
          <i className="feather-search font-sm text-grey-400"></i>
          <input type="text" placeholder="Start typing to search.." className="bg-grey border-0 lh-32 pt-2 pb-2 ps-5 pe-3 font-xssss fw-500 rounded-xl w350 theme-dark-bg"/>
        </Box>
      </form>
      
      <Link to={"/"} className="p-2 text-center ms-3 menu-icon center-menu-icon">
        <i className="feather-home font-lg alert-primary btn-round-lg theme-dark-bg text-current "></i>
      </Link>
      <Link to={"storie"} className="p-2 text-center ms-0 menu-icon center-menu-icon">
        <i className="feather-zap font-lg bg-greylight btn-round-lg theme-dark-bg text-grey-500 "></i>
      </Link>
      <Link to={"video"} className="p-2 text-center ms-0 menu-icon center-menu-icon">
        <i className="feather-video font-lg bg-greylight btn-round-lg theme-dark-bg text-grey-500 "></i>
      </Link>
      <Link to={"group"} className="p-2 text-center ms-0 menu-icon center-menu-icon">
        <i className="feather-user font-lg bg-greylight btn-round-lg theme-dark-bg text-grey-500 "></i>
      </Link>
      <Link to={"shop-2.html"} className="p-2 text-center ms-0 menu-icon center-menu-icon">
        <i className="feather-shopping-bag font-lg bg-greylight btn-round-lg theme-dark-bg text-grey-500 "></i>
      </Link>
      
      <Notification />
      
      <Link to={"#"} className="p-2 text-center ms-3 menu-icon chat-active-btn">
        <i className="feather-message-square font-xl text-current"></i>
      </Link>
      
      <Setting />
      
      <Link to={"settings"} className="p-0 ms-3 menu-icon">
        <img src="/assets/images/profile-4.png" alt="user" className="w40 mt--1" />
      </Link>
      
    </Box>
  );
};

export default Header;
