import React, {useState} from "react";

const Notification: React.FC = () => {
  const [isDropdownOpen, setDropdownOpen] = useState(false);
  
  const toggleDropdown = () => {
    setDropdownOpen(!isDropdownOpen);
  };
  
  return (
    <React.Fragment>
      <a
        href="#"
        className={`p-2 text-center ms-auto menu-icon ${isDropdownOpen ? 'show' : ''}`}
        id="dropdownMenu3"
        onClick={toggleDropdown}
        aria-haspopup="true"
        aria-expanded={isDropdownOpen ? "true" : "false"}
        data-bs-toggle="dropdown"
      >
        <span className="dot-count bg-warning"></span>
        <i className="feather-bell font-xl text-current"></i>
      </a>
      
      <div
        className={`dropdown-menu dropdown-menu-end p-4 rounded-3 border-0 shadow-lg ${isDropdownOpen ? 'show' : ''}`}
        aria-labelledby="dropdownMenu3"
      >
        <h4 className="fw-700 font-xss mb-4">Notification</h4>
        
        <div className="card bg-transparent-card w-100 border-0 ps-5 mb-3">
          <img src="/assets/images/user-8.png" alt="user" className="w40 position-absolute left-0"/>
          <h5 className="font-xsss text-grey-900 mb-1 mt-0 fw-700 d-block">
            Hendrix Stamp
            <span className="text-grey-400 font-xsssss fw-600 float-right mt-1"> 3 min</span>
          </h5>
          <h6 className="text-grey-500 fw-500 font-xssss lh-4">There are many variations of pass..</h6>
        </div>
        
        <div className="card bg-transparent-card w-100 border-0 ps-5 mb-3">
          <img src="/assets/images/user-4.png" alt="user" className="w40 position-absolute left-0"/>
          <h5 className="font-xsss text-grey-900 mb-1 mt-0 fw-700 d-block">Goria Coast <span className="text-grey-400 font-xsssss fw-600 float-right mt-1"> 2 min</span></h5>
          <h6 className="text-grey-500 fw-500 font-xssss lh-4">Mobile Apps UI Designer is require..</h6>
        </div>
        
        <div className="card bg-transparent-card w-100 border-0 ps-5 mb-3">
          <img src="/assets/images/user-7.png" alt="user" className="w40 position-absolute left-0"/>
          <h5 className="font-xsss text-grey-900 mb-1 mt-0 fw-700 d-block">Surfiya Zakir <span className="text-grey-400 font-xsssss fw-600 float-right mt-1"> 1 min</span></h5>
          <h6 className="text-grey-500 fw-500 font-xssss lh-4">Mobile Apps UI Designer is require..</h6>
        </div>
        <div className="card bg-transparent-card w-100 border-0 ps-5">
          <img src="/assets/images/user-6.png" alt="user" className="w40 position-absolute left-0"/>
          <h5 className="font-xsss text-grey-900 mb-1 mt-0 fw-700 d-block">
            Victor Exrixon
            <span className="text-grey-400 font-xsssss fw-600 float-right mt-1">
              30 sec
            </span>
          </h5>
          <h6 className="text-grey-500 fw-500 font-xssss lh-4">Mobile Apps UI Designer is require..</h6>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Notification;
