import React from "react";
import {Link} from "react-router-dom";

import Modal from "@mui/material/Modal";
import {Backdrop} from "@mui/material";
import Fade from "@mui/material/Fade";
import Box from "@mui/material/Box";

import StatusView from "@/pages/user/home/MainContent/cards/Status/StatusView.tsx";

export const StatusCard = ({item}) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  return (
    <React.Fragment>
      <Box
        className="card w125 h200 d-block border-0 shadow-xss rounded-xxxl bg-gradiant-bottom overflow-hidden cursor-pointer mb-3 mt-3"
        style={{backgroundImage: `url(${item.background})`}}
      >
        <Box className="card-body d-block p-3 w-100 position-absolute bottom-0 text-center">
          <Link to={"#"}>
            <figure className="avatar ms-auto me-auto mb-0 position-relative w50 z-index-1">
              <img
                src={item.avatar}
                alt="image"
                className="float-right p-0 bg-white rounded-circle w-100 shadow-xss"
                style={{opacity: 1}}
                onClick={handleOpen}
              />
            </figure>
            <Box className="clearfix"/>
            <h4 className="fw-600 position-relative z-index-1 ls-1 font-xssss text-white mt-2 mb-1">
              {item.title}
            </h4>
          </Link>
        </Box>
      </Box>
      
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{backdrop: Backdrop}}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            height="100vh"
          >
            <StatusView item={{ id: item.id }} />
          </Box>
        </Fade>
      </Modal>
    </React.Fragment>
  );
};