import React from "react";
import {Link} from "react-router-dom";

import {Backdrop} from "@mui/material";
import Fade from "@mui/material/Fade";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

export const StatusAdd = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  return (
    <React.Fragment>
      <div
        className="card w125 h200 d-block border-0 shadow-none rounded-xxxl bg-dark overflow-hidden mb-3 mt-3"
      >
        <div className="card-body d-block p-3 w-100 position-absolute bottom-0 text-center">
          <Link to={""}>
            <span className="btn-round-lg bg-white" onClick={handleOpen}>
              <i className="feather-plus font-lg"/>
            </span>
            <div className="clearfix"/>
            <h4
              className="fw-700 position-relative z-index-1 ls-1 font-xssss text-white mt-2 mb-1"
            >
              Add Story
            </h4>
          </Link>
        </div>
      </div>
      
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{backdrop: Backdrop}}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box>
          </Box>
        </Fade>
      </Modal>
    </React.Fragment>
  )
};