import React, { useState } from 'react';

import {Box, Icon} from '@mui/material';
import { Modal } from 'antd';

import Core from "./comment/Core";
import { CommentProvider } from "./comment/commentContext";

const CommentSection = ({ meta }) => {
  
  const [open, setOpen] = useState(false);
  
  return (
    <React.Fragment>
      <Box
        display="flex"
        alignItems="center"
        fontWeight={600}
        color="text.primary"
        lineHeight="26px"
        fontSize="x-small"
        sx={{
          '&:hover': {
            cursor: 'pointer',
          },
        }}
        onClick={() => setOpen(true)}
      >
        <Icon
          className="feather-message-circle text-dark text-grey-900 btn-round-sm font-lg"
          sx={{ fontSize: '1.2rem' }}
        />
        <span className="d-none-xss">{meta.comment} Comment</span>
      </Box>
      
      <Modal
        title="Comments"
        open={open}
        footer={null}
        onCancel={() => setOpen(!open)}
        width={1000}
      >
        <CommentProvider>
          <Core />
        </CommentProvider>
      </Modal>
    </React.Fragment>
  );
};

export default CommentSection;
