import React from "react";

import Share from "@/pages/user/home/MainContent/cards/Post/Share.tsx";
import Comment from "@/pages/user/home/MainContent/cards/Post/Comment.tsx";
import Reaction from "@/pages/user/home/MainContent/cards/Post/Reaction.tsx";

export const Footer = ({ meta }) => {
  return (
    <>
      <Reaction meta={meta} />
      <Comment meta={meta} />
      <Share meta={meta} />
    </>
  );
};

export default Footer;