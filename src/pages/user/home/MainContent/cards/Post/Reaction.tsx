import React, { useState, useEffect } from "react";

import { Tooltip } from "@mui/material";
import { Grid } from "@mui/material";

import { Popover } from 'antd';
import Box from "@mui/material/Box";

const EmojiWrap = () => {
  return (
    <Grid
      container
      spacing={1}
      sx={{
        '&:hover': {
          cursor: 'pointer',
        },
      }}
    >
      <Grid item>
        <Tooltip title="Like" placement="top">
          <span style={{fontSize: '25px'}}>&#128077;</span>
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Love" placement="top">
          <span style={{fontSize: '25px'}}>❤️</span>
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Haha" placement="top">
          <span style={{fontSize: '25px'}}>🤣</span>
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Angry" placement="top">
          <span style={{fontSize: '25px'}}>👿</span>
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Wow" placement="top">
          <span style={{fontSize: '25px'}}>😲</span>
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Celebrate" placement="top">
          <span style={{fontSize: '25px'}}>👏</span>
        </Tooltip>
      </Grid>
    </Grid>
  );
};

export const Reaction = ({ meta }) => {
  return (
    <React.Fragment>
      <Box
        className="emoji-bttn d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"
        sx={{
          '&:hover': {
            cursor: 'pointer',
          },
        }}
      >
        <Tooltip title="Like" placement="top">
          <i className="feather-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss" />
        </Tooltip>
        <Popover placement="topLeft" content={<EmojiWrap />} trigger="click">
          <i className="feather-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"/>
        </Popover>
        {meta.like} Like
      </Box>
    </React.Fragment>
  );
};

export default Reaction;
