import React from "react";

import {MoreMenu} from "@/layout/SocialLayout/components/MoreMenu.tsx";
import {humanReadable} from "@/utils/dayjs.ts";
import { Link } from "react-router-dom";
import Footer from "@/pages/user/home/MainContent/cards/Post/Footer.tsx";

interface PhotoProps {
  post: any
}

export const Photo: React.FC<PhotoProps> = ({ post }) => {
  return (
    <React.Fragment>
      <div className="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-3">
        <div className="card-body p-0 d-flex">
          <figure className="avatar me-3">
            <img
              src="/assets/images/user-7.png"
              alt="image"
              className="shadow-sm rounded-circle w45"
            />
          </figure>
          <h4 className="fw-700 text-grey-900 font-xssss mt-1">
            {post.name}
            <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
              {humanReadable(post.timestamp)}
            </span>
          </h4>
          <div className="ms-auto">
            <MoreMenu />
          </div>
        </div>
        
        <div className="card-body p-0 me-lg-5">
          <p className="fw-500 text-grey-500 lh-26 font-xssss w-100">
            {post.description}
            <a href="#" className="fw-600 text-primary ms-2">
              See more
            </a>
          </p>
        </div>
        
        <Images medias={post.photos} />
        
        <div className="card-body d-flex p-0 mt-3">
          <Footer meta={post} />
        </div>
      </div>
    </React.Fragment>
  );
};

const Images = ({ medias }) => {
  switch (medias.length) {
    case 5:
    case 6:
      return (
        <React.Fragment>
          <div className="card-body d-block p-0">
            <div className="row ps-2 pe-2">
              {(() => {
                const photoElements = [];
                for (let index = 0; index < 2; index++) {
                  const photo = medias[index];
                  
                  photoElements.push(
                    <div className="col-xs-4 col-sm-4 p-1" key={index}>
                      <a href={photo} data-lightbox="roadtrip">
                        <img
                          src={photo}
                          className="rounded-3 w-100"
                          alt="image"
                        />
                      </a>
                    </div>
                  );
                }
                return photoElements;
              })()}
              
              <div className="col-xs-4 col-sm-4 p-1">
                <a
                  href={medias[2]}
                  data-lightbox="roadtrip"
                  className="position-relative d-block"
                >
                  <img
                    src="/assets/images/t-12.jpg"
                    className="rounded-3 w-100"
                    alt="image"
                  />
                  <span className="img-count font-sm text-white ls-3 fw-600">
                    <b>+{medias.length - 3}</b>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    default:
      return (
        <React.Fragment>
          <div className="card-body d-block p-0">
            <div className="row ps-2 pe-2">
              {(() => {
                const photoElements = [];
                for (let index = 0; index < 2; index++) {
                  const photo = medias[index];
                  
                  photoElements.push(
                    <div className="col-xs-4 col-sm-4 p-1" key={index}>
                      <a href={photo} data-lightbox="roadtrip">
                        <img
                          src={photo}
                          className="rounded-3 w-100"
                          alt="image"
                        />
                      </a>
                    </div>
                  );
                }
                return photoElements;
              })()}
              
              <div className="col-xs-4 col-sm-4 p-1">
                <a
                  href={medias[2]}
                  data-lightbox="roadtrip"
                  className="position-relative d-block"
                >
                  <img
                    src="/assets/images/t-12.jpg"
                    className="rounded-3 w-100"
                    alt="image"
                  />
                  <span className="img-count font-sm text-white ls-3 fw-600">
                    <b>+{medias.length - 3}</b>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
  }
};


export default Photo;