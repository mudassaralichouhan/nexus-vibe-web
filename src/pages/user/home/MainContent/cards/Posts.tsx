import React from "react";

import Photo from './Post/Photo.tsx';
import Text from './Post/Text.tsx';
import Live from "./Live";
import YouMayKnow from "@/pages/user/home/MainContent/cards/YouMayKnow.tsx";
import Video from "@/pages/user/home/MainContent/cards/Post/Video.tsx";

const list = [
  {
    name: 'Surfiya Zakir',
    timestamp: '2023-08-19 11:11:00',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus',
    photos: [
      '/assets/images/t-10.jpg',
      '/assets/images/t-11.jpg',
      '/assets/images/t-12.jpg',
      '/assets/images/t-12.jpg',
      '/assets/images/t-12.jpg',
      '/assets/images/t-12.jpg',
    ],
    like: 5490,
    comment: 1270,
  },
  {
    name: 'Surfiya Zakir',
    timestamp: '2023-06-19 11:11:00',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus faucibus mollis pharetra. Proin blandit ac massa sed rhoncus',
    photos: [
      '/assets/images/t-21.jpg',
      '/assets/images/t-22.jpg',
      '/assets/images/t-23.jpg',
      '/assets/images/t-24.jpg',
      '/assets/images/t-25.jpg',
    ],
    like: 5490,
    comment: 1270,
  }
];

const Posts: React.FC = () => {
  return (
    <React.Fragment>
      <Photo post={list[0]} />
      <Photo post={list[1]} />
      {/*<Text/>
      <Live/>
      <Video/>*/}
      
      <div className="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-0">
        <div className="card-body p-0 d-flex">
          <figure className="avatar me-3">
            <img
              src="/assets/images/user-8.png"
              alt="image"
              className="shadow-sm rounded-circle w45"
            />
          </figure>
          <h4 className="fw-700 text-grey-900 font-xssss mt-1">
            Anthony Daugloi
            <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
                  2 hour ago
                </span>
          </h4>
          <a href="#" className="ms-auto">
            <i className="ti-more-alt text-grey-900 btn-round-md bg-greylight font-xss"/>
          </a>
        </div>
        
        <div className="card-body p-0 me-lg-5">
          <p className="fw-500 text-grey-500 lh-26 font-xssss w-100">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
            nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus
            faucibus mollis pharetra. Proin blandit ac massa sed rhoncus
            <a href="#" className="fw-600 text-primary ms-2">
              See more
            </a>
          </p>
        </div>
        
        <div className="card-body d-block p-0 mb-3">
          <div className="row ps-2 pe-2">
            <div className="col-xs-6 col-sm-6 p-1">
              <a href="/assets/images/t-36.jpg" data-lightbox="roadtri">
                <img
                  src="/assets/images/t-21.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
              </a>
            </div>
            <div className="col-xs-6 col-sm-6 p-1">
              <a href="/assets/images/t-32.jpg" data-lightbox="roadtri">
                <img
                  src="/assets/images/t-22.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
              </a>
            </div>
          </div>
          <div className="row ps-2 pe-2">
            <div className="col-xs-4 col-sm-4 p-1">
              <a href="/assets/images/t-33.jpg" data-lightbox="roadtri">
                <img
                  src="/assets/images/t-23.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
              </a>
            </div>
            <div className="col-xs-4 col-sm-4 p-1">
              <a href="/assets/images/t-34.jpg" data-lightbox="roadtri">
                <img
                  src="/assets/images/t-24.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
              </a>
            </div>
            <div className="col-xs-4 col-sm-4 p-1">
              <a
                href="/assets/images/t-35.jpg"
                data-lightbox="roadtri"
                className="position-relative d-block"
              >
                <img
                  src="/assets/images/t-25.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
                <span className="img-count font-sm text-white ls-3 fw-600">
                      <b>+2</b>
                    </span>
              </a>
            </div>
          </div>
        </div>
        <div className="card-body d-flex p-0">
          <a
            href="#"
            className="emoji-bttn d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"
          >
            <i className="feather-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"/>
            <i className="feather-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"/>
            2.8K Like
          </a>
          <div className="emoji-wrap">
            <ul className="emojis list-inline mb-0">
              <li className="emoji list-inline-item">
                <i className="em em---1"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-angry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-anguished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-astonished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-blush"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-clap"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-cry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-full_moon_with_face"/>
              </li>
            </ul>
          </div>
          <a
            href="#"
            className="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-message-circle text-dark text-grey-900 btn-round-sm font-lg"/>
            <span className="d-none-xss">22 Comment</span>
          </a>
          <a
            href="#"
            className="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-share-2 text-grey-900 text-dark btn-round-sm font-lg"/>
            <span className="d-none-xs">Share</span>
          </a>
        </div>
      </div>
      
      <YouMayKnow/>
      
      <div className="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-3">
        <div className="card-body p-0 d-flex">
          <figure className="avatar me-3">
            <img
              src="/assets/images/user-8.png"
              alt="image"
              className="shadow-sm rounded-circle w45"
            />
          </figure>
          <h4 className="fw-700 text-grey-900 font-xssss mt-1">
            Anthony Daugloi
            <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
                  2 hour ago
                </span>
          </h4>
          <a href="#" className="ms-auto">
            <i className="ti-more-alt text-grey-900 btn-round-md bg-greylight font-xss"/>
          </a>
        </div>
        <div className="card-body p-0 mb-3 rounded-3 overflow-hidden">
          <a href="default-video.html" className="video-btn">
            <video className="float-right w-100">
              <source src="/assets/images/v-1.mp4" type="video/mp4"/>
            </video>
          </a>
        </div>
        <div className="card-body p-0 me-lg-5">
          <p className="fw-500 text-grey-500 lh-26 font-xssss w-100 mb-2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
            nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus
            faucibus mollis pharetra. Proin blandit ac massa sed rhoncus
            <a href="#" className="fw-600 text-primary ms-2">
              See more
            </a>
          </p>
        </div>
        <div className="card-body d-flex p-0">
          <a
            href="#"
            className="emoji-bttn d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"
          >
            <i className="feather-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"/>
            <i className="feather-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"/>
            2.8K Like
          </a>
          <div className="emoji-wrap">
            <ul className="emojis list-inline mb-0">
              <li className="emoji list-inline-item">
                <i className="em em---1"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-angry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-anguished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-astonished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-blush"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-clap"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-cry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-full_moon_with_face"/>
              </li>
            </ul>
          </div>
          <a
            href="#"
            className="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-message-circle text-dark text-grey-900 btn-round-sm font-lg"/>
            <span className="d-none-xss">22 Comment</span>
          </a>
          <a
            href="#"
            className="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-share-2 text-grey-900 text-dark btn-round-sm font-lg"/>
            <span className="d-none-xs">Share</span>
          </a>
        </div>
      </div>
      <div className="card w-100 shadow-xss rounded-xxl border-0 p-4 mb-0">
        <div className="card-body p-0 d-flex">
          <figure className="avatar me-3">
            <img
              src="/assets/images/user-8.png"
              alt="image"
              className="shadow-sm rounded-circle w45"
            />
          </figure>
          <h4 className="fw-700 text-grey-900 font-xssss mt-1">
            Anthony Daugloi
            <span className="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">
                  2 hour ago
                </span>
          </h4>
          <a href="#" className="ms-auto">
            <i className="ti-more-alt text-grey-900 btn-round-md bg-greylight font-xss"/>
          </a>
        </div>
        <div className="card-body p-0 me-lg-5">
          <p className="fw-500 text-grey-500 lh-26 font-xssss w-100 mb-2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
            nulla dolor, ornare at commodo non, feugiat non nisi. Phasellus
            faucibus mollis pharetra. Proin blandit ac massa sed rhoncus
            <a href="#" className="fw-600 text-primary ms-2">
              See more
            </a>
          </p>
        </div>
        <div className="card-body d-block p-0 mb-3">
          <div className="row ps-2 pe-2">
            <div className="col-sm-12 p-1">
              <a href="/assets/images/t-30.jpg" data-lightbox="roadtr">
                <img
                  src="/assets/images/t-31.jpg"
                  className="rounded-3 w-100"
                  alt="image"
                />
              </a>
            </div>
          </div>
        </div>
        <div className="card-body d-flex p-0">
          <a
            href="#"
            className="emoji-bttn d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss me-2"
          >
            <i className="feather-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"/>
            <i className="feather-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"/>
            2.8K Like
          </a>
          <div className="emoji-wrap">
            <ul className="emojis list-inline mb-0">
              <li className="emoji list-inline-item">
                <i className="em em---1"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-angry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-anguished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-astonished"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-blush"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-clap"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-cry"/>
              </li>
              <li className="emoji list-inline-item">
                <i className="em em-full_moon_with_face"/>
              </li>
            </ul>
          </div>
          <a
            href="#"
            className="d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-message-circle text-dark text-grey-900 btn-round-sm font-lg"/>
            <span className="d-none-xss">22 Comment</span>
          </a>
          <a
            href="#"
            className="ms-auto d-flex align-items-center fw-600 text-grey-900 text-dark lh-26 font-xssss"
          >
            <i className="feather-share-2 text-grey-900 text-dark btn-round-sm font-lg"/>
            <span className="d-none-xs">Share</span>
          </a>
        </div>
      </div>
      <div className="card w-100 text-center shadow-xss rounded-xxl border-0 p-4 mb-3 mt-3">
        <div
          className="snippet mt-2 ms-auto me-auto"
          data-title=".dot-typing"
        >
          <div className="stage">
            <div className="dot-typing"/>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Posts;
