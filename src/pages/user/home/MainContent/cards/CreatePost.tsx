import React from "react";
import { Link } from 'react-router-dom';

import {Card, CardContent, CardActions, TextareaAutosize} from "@mui/material";
import { MoreMenu } from "@/layout/SocialLayout/components/MoreMenu";

import PhotoVideo from "@/pages/user/home/MainContent/cards/CreatePost/PhotoVideo.tsx";
import LiveVideo from "@/pages/user/home/MainContent/cards/CreatePost/LiveVideo.tsx";
import FeelingActivity from "@/pages/user/home/MainContent/cards/CreatePost/FeelingActivity.tsx";

const CreatePost: React.FC = () => {
  
  return (
    <Card variant="outlined" className="w-100 shadow-xss rounded-xxl border-0 ps-4 pt-4 pe-4 pb-3 mb-3">
      <CardContent className="p-0">
        <Link to={""} className="font-xssss fw-600 text-grey-500 card-body p-0 d-flex align-items-center">
          <i className="btn-round-sm font-xs text-primary feather-edit-3 me-2 bg-greylight"></i>
          Create Post
        </Link>
      </CardContent>
      <CardContent className="p-0 mt-3 position-relative">
        <figure className="avatar position-absolute ms-2 mt-1 top-5">
          <img src="/assets/images/user-8.png" alt="image" className="shadow-sm rounded-circle w30"/>
        </figure>
        <TextareaAutosize
          name="message"
          className="h100 bor-0 w-100 rounded-xxl p-2 ps-5 font-xssss text-grey-500 fw-500 border-light-md theme-dark-bg"
          placeholder="What's on your mind?"
          defaultValue={""}
        />
      </CardContent>
      <CardActions className="d-flex p-0 mt-0">
        <LiveVideo />
        <PhotoVideo />
        <FeelingActivity />
        <div className="ms-auto">
          <MoreMenu />
        </div>
      </CardActions>
    </Card>
  );
};

export default CreatePost;
