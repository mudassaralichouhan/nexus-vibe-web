import React from "react";

import BearCarousel from "bear-react-carousel";
import "bear-react-carousel/dist/index.css";

import { StatusAdd } from "./Status/StatusAdd";
import {StatusCard} from "./Status/StatusCard.tsx";

const cardList = [
  {
    id: 1,
    background: 'https://picsum.photos/1080/1920',
    avatar: 'https://picsum.photos/300/300',
    title: 'Victor Exrixon',
  },
  {
    id: 2,
    background: 'https://picsum.photos/1080/1920',
    avatar: 'https://picsum.photos/300/300',
    title: 'Victor Exrixon',
  },
  {
    id: 3,
    background: 'https://picsum.photos/1080/1920',
    avatar: 'https://picsum.photos/300/300',
    title: 'Victor Exrixon',
  },
  {
    id: 4,
    background: 'https://picsum.photos/1080/1920',
    avatar: 'https://picsum.photos/300/300',
    title: 'Victor Exrixon',
  },
  {
    id: 5,
    background: 'https://picsum.photos/1080/1920',
    avatar: 'https://picsum.photos/300/300',
    title: 'Victor Exrixon',
  },
];

const StatusSlider = () => {
  
  const statusSliderList = cardList.map((row) => {
    return {
      id: row.id,
      children: <StatusCard item={row} />,
    };
  });
  
  statusSliderList.unshift({
    id: '9q334rn',
    children: <StatusAdd />
  })
  
  return (
    <React.Fragment>
      <BearCarousel
        data={statusSliderList}
        slidesPerView={5}
        height={{widthRatio: 1, heightRatio: 0.4}}
      />
    </React.Fragment>
  );
};

export default StatusSlider;
