import React from "react";

const Live: React.FC = () => {
  return (
    <div className="card w-100 shadow-none bg-transparent bg-transparent-card border-0 p-0 mb-0">
      <div className="owl-carousel category-card owl-theme overflow-hidden nav-none owl-loaded owl-drag">
        <div className="owl-stage-outer">
          <div
            className="owl-stage"
            style={{
              transform: "translate3d(0px, 0px, 0px)",
              transition: "all 0s ease 0s",
              width: 1505
            }}
          >
            <div
              className="owl-item active"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/u-bg.jpg)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-11.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Aliqa Macale
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item active"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/s-2.jpg)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-2.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Seary Victor
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item active"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/s-6.jpg)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-3.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      John Steere
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item active"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/bb-16.png)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-4.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Mohannad Zitoun
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/e-4.jpg)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-7.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Studio Express
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{
                      backgroundImage: "url(/assets/images/coming-soon.png)"
                    }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-5.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Hendrix Stamp
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
            <div
              className="owl-item"
              style={{ width: "auto", marginRight: 7 }}
            >
              <div className="item">
                <div className="card w200 d-block border-0 shadow-xss rounded-xxl overflow-hidden mb-3 me-2 mt-3">
                  <div
                    className="card-body position-relative h100 bg-image-cover bg-image-center"
                    style={{ backgroundImage: "url(/assets/images/bb-9.jpg)" }}
                  />
                  <div className="card-body d-block w-100 ps-4 pe-4 pb-4 text-center">
                    <figure className="avatar ms-auto me-auto mb-0 mt--6 position-relative w75 z-index-1">
                      <img
                        src="/assets/images/user-6.png"
                        alt="image"
                        className="float-right p-1 bg-white rounded-circle w-100"
                        style={{ opacity: 1 }}
                      />
                    </figure>
                    <div className="clearfix" />
                    <h4 className="fw-700 font-xsss mt-2 mb-1">
                      Mohannad Zitoun
                    </h4>
                    <p className="fw-500 font-xsssss text-grey-500 mt-0 mb-2">
                      support@gmail.com
                    </p>
                    <span className="live-tag mt-2 mb-0 bg-danger p-2 z-index-1 rounded-3 text-white font-xsssss text-uppersace fw-700 ls-3">
                            LIVE
                          </span>
                    <div className="clearfix mb-2" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="owl-nav">
          <button
            type="button"
            role="presentation"
            className="owl-prev disabled"
          >
            <i className="ti-angle-left" />
          </button>
          <button type="button" role="presentation" className="owl-next">
            <i className="ti-angle-right" />
          </button>
        </div>
        <div className="owl-dots disabled" />
      </div>
    </div>
  );
};

export default Live;
