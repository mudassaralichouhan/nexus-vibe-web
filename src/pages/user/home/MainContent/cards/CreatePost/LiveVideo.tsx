import React from "react";

export const LiveVideo = () => {
  return (
    <React.Fragment>
      <a href="" className="d-flex align-items-center font-xssss fw-600 ls-1 text-grey-700 text-dark pe-4">
        <i className="font-md text-danger feather-video me-2"/>
        <span className="d-none-xs">Live Video</span>
      </a>
    </React.Fragment>
  );
};

export default LiveVideo;