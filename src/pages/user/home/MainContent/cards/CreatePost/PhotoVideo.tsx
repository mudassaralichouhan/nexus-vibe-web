import React from "react";

export const PhotoVideo = () => {
  return (
    <React.Fragment>
      <a href="#" className="d-flex align-items-center font-xssss fw-600 ls-1 text-grey-700 text-dark pe-4">
        <i className="font-md text-success feather-image me-2" />
        <span className="d-none-xs">Photo/Video</span>
      </a>
    </React.Fragment>
  );
};

export default PhotoVideo;