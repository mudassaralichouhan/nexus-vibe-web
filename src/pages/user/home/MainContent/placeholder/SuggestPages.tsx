import React from "react";
import {Link} from "react-router-dom";

const SuggestPages: React.FC = () => {
  const pages = [
    {
      image: "/assets/images/g-2.jpg",
      url: "link-to-page-1",
    },
    {
      image: "/assets/images/g-3.jpg",
      url: "link-to-page-2",
    },
  ];
  
  return (
    <div className="card w-100 shadow-xss rounded-xxl border-0 mb-3">
      <div className="card-body d-flex align-items-center p-4">
        <h4 className="fw-700 mb-0 font-xssss text-grey-900">Suggest Pages</h4>
        <Link to={"/pages"} className="fw-600 ms-auto font-xssss text-primary">See all</Link>
      </div>
      {pages.map((page, index) => (
        <React.Fragment key={index}>
          <div className={`card-body d-flex ps-4 pe-4 pb-0 overflow-hidden ${index === 0 ? 'border-top-xs bor-0' : ''}`}>
            <img
              src={page.image}
              alt="img"
              className="img-fluid rounded-xxl mb-2 bg-lightblue"
            />
          </div>
          <div className="card-body d-flex align-items-center pt-0 ps-4 pe-4 pb-4">
            <a
              href={page.url}
              className="p-2 lh-28 w-100 bg-grey text-grey-800 text-center font-xssss fw-700 rounded-xl"
            >
              <i className="feather-external-link font-xss me-2"/> Like Page
            </a>
          </div>
        </React.Fragment>
      ))}
    </div>
  );
};

export default SuggestPages;
