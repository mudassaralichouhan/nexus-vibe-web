import React from "react";

import Avatar from '@mui/material/Avatar';
import AvatarGroup from '@mui/material/AvatarGroup';
import {Card} from "antd";
import { Link } from "react-router-dom";

const SuggestGroup: React.FC = () => {
  return (
    <div className="card w-100 shadow-xss rounded-xxl border-0 mb-3 mt-3">
      <div className="card-body d-flex align-items-center p-4">
        <h4 className="fw-700 mb-0 font-xssss text-grey-900">
          Suggest Group
        </h4>
        <Link
          to={"groups"}
          className="fw-600 ms-auto font-xssss text-primary"
        >
          See all
        </Link>
      </div>
      <div className="card-body d-flex pt-4 ps-4 pe-4 pb-0 overflow-hidden border-top-xs bor-0">
        <img
          src="/assets/images/e-2.jpg"
          alt="img"
          className="img-fluid rounded-xxl mb-2"
        />
      </div>
      
      <Card style={{ display: 'flex', alignItems: 'center' }}>
        <AvatarGroup max={6}>
          <Avatar alt="Remy Sharp" src="/assets/images/user-6.png"/>
          <Avatar alt="Travis Howard" src="/assets/images/user-3.png"/>
          <Avatar alt="Cindy Baker" src="/assets/images/user-7.png"/>
          <Avatar alt="Agnes Walker" src="/assets/images/user-8.png"/>
          <Avatar alt="Trevor Henderson" src="/assets/images/user-3.png"/>
          <Avatar alt="Trevor Henderson" src="/assets/images/user-3.png"/>
          <Avatar alt="Trevor Henderson" src="/assets/images/user-3.png"/>
          <Avatar alt="Trevor Henderson" src="/assets/images/user-3.png"/>
          <Avatar alt="Trevor Henderson" src="/assets/images/user-3.png"/>
        </AvatarGroup>
        <Link to={""} className="fw-600 text-grey-500 font-xssss">
          Member apply
        </Link>
      </Card>

    </div>
  );
};

export default SuggestGroup;
