import {api, ResponseType} from "@/api";
import {AxiosResponse} from "axios";

export interface RegisterType {
  email: string
  password: string
}

export const registerApi = async (formData: RegisterType): Promise<ResponseType> => {
  try {
    const response: AxiosResponse = await api.post('/auth/register', formData);

    return {
      data: response.data,
      error: false,
    };

  } catch (error) {
    return {
      data: error?.response?.data || error.message,
      error: true,
    };
  }
}