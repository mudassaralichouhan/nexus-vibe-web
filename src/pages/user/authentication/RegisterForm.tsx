import React from "react";
import {FieldValues, useForm} from 'react-hook-form';
import {ResponseType} from "@/api";
import {toast} from 'react-toastify';
import {registerApi, RegisterType} from "@/pages/user/authentication/register-api.ts";

const LoginForm: React.FC = () => {

  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm();

  const handleValidation = async (data: FieldValues) => {
    const result: ResponseType = await registerApi(data as RegisterType);

    if (!result.error) {
      toast.success("register success fully !", {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    } else {
      if (Array.isArray(result.data))
        result.data.map((m) => {
          toast.error(m.message || "register fail !");
        })
      toast.error(result.data?.error || "register fail !");
    }
  }

  return (
    <form onSubmit={handleSubmit(handleValidation)}>
      <h2 className="fw-700 display1-size display2-md-size mb-3">
        Login into <br/> your account
      </h2>

      <div className="form-group icon-input mb-3">
        <i className="font-sm ti-user text-grey-500 pe-0"/>
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xsss fw-600"
          {...register("name", {
            required: "required",
            maxLength: {
              value: 25,
              message: "max length is 25"
            }
          })}
          placeholder="Your Name"
          type="text"
        />
        {errors.email && <span className="text-danger">{errors.name.message}</span>}
      </div>

      <div className="form-group icon-input mb-3">
        <i className="font-sm ti-email text-grey-500 pe-0"/>
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xsss fw-600"
          {...register("email", {
            required: "required",
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Entered value does not match email format"
            },
            maxLength: {
              value: 25,
              message: "max length is 25"
            }
          })}
          placeholder={'Your Email Address'}
          type="email"
        />
        {errors.email && <span className="text-danger">{errors.email.message}</span>}
      </div>
      <div className="form-group icon-input mb-1">
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xss ls-3"
          {...register("password", {
            required: "required",
            minLength: {
              value: 6,
              message: "min length is 6"
            }
          })}
          placeholder={'Password'}
          type="password"
        />
        {errors.password && <span className="text-danger">{errors?.password?.message}</span>}
        <i className="font-sm ti-lock text-grey-500 pe-0"/>
      </div>
      <div className="form-group icon-input mb-1">
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xss ls-3"
          {...register("repeat_password", {
            required: "required",
            minLength: {
              value: 6,
              message: "min length is 6"
            }
          })}
          placeholder={'Confirm Password'}
          type="password"
        />
        {errors.password && <span className="text-danger">{errors?.repeat_password?.message}</span>}
        <i className="font-sm ti-lock text-grey-500 pe-0"/>
      </div>

      <div className="form-check text-left mb-3">
        <input
          className="form-check-input mt-2"
          {...register("terms", {
            required: "required",
          })}
          type="checkbox"
        />
        <label
          className="form-check-label font-xsss text-grey-500"
        >
          Accept Term and Conditions
        </label>
        {errors.password && <span className="text-danger d-block">{errors?.terms?.message}</span>}
      </div>

      <div className="col-sm-12 p-0 text-left">
        <div className="form-group mb-1">
          <button
            type={'submit'}
            className="form-control text-center style2-input text-white fw-600 bg-dark border-0 p-0 "
          >
            Register
          </button>
        </div>
      </div>
    </form>
  );
}

export default LoginForm;