import React from "react";
import {Link, useNavigate} from "react-router-dom";
import {FieldValues, useForm} from 'react-hook-form';
import {loginApi, LoginType} from "@/pages/user/authentication/login-api.ts";
import {ResponseType} from "@/api";
import {toast} from 'react-toastify';

const LoginForm: React.FC = () => {

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm();

  const handleValidation = async (data: FieldValues) => {
    const result: ResponseType = await loginApi(data as LoginType);

    if (!result.error) {
      toast.success("login success fully !", {
        position: toast.POSITION.BOTTOM_RIGHT
      });
      navigate('/');
    } else {
      toast.error(result.data?.error || "login fail !");
    }
  }

  return (
    <form onSubmit={handleSubmit(handleValidation)}>
      <h2 className="fw-700 display1-size display2-md-size mb-3">
        Login into <br/> your account
      </h2>
      <div className="form-group icon-input mb-3">
        <i className="font-sm ti-email text-grey-500 pe-0"/>
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xsss fw-600"
          {...register("email", {
            required: "required",
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Entered value does not match email format"
            },
            maxLength: {
              value: 25,
              message: "max length is 25"
            }
          })}
          type="email"
          value={'ryzuwur@mailinator.com'}
        />
        {errors.email && <span className="text-danger">{errors.email.message}</span>}
      </div>
      <div className="form-group icon-input mb-1">
        <input
          className="style2-input ps-5 form-control text-grey-900 font-xss ls-3"
          {...register("password", {
            required: "required",
            minLength: {
              value: 6,
              message: "min length is 6"
            }
          })}
          type="password"
        />
        {errors.password && <span className="text-danger">{errors?.password?.message}</span>}
        <i className="font-sm ti-lock text-grey-500 pe-0"/>
      </div>
      <div className="form-check text-left mb-3">
        <input
          type="checkbox"
          className="form-check-input mt-2"
          id="exampleCheck5"
        />
        <label
          className="form-check-label font-xsss text-grey-500"
          htmlFor="exampleCheck5"
        >
          Remember me
        </label>
        <Link
          to={'/auth/password/forgot'}
          className="fw-600 font-xsss text-grey-700 mt-1 float-right"
        >
          Forgot your Password?
        </Link>
      </div>
      <div className="col-sm-12 p-0 text-left">
        <div className="form-group mb-1">
          <button
            type={'submit'}
            className="form-control text-center style2-input text-white fw-600 bg-dark border-0 p-0"
          >
            Login
          </button>
        </div>
      </div>
    </form>
  );
}

export default LoginForm;