import {Link} from 'react-router-dom';
import React from "react";
import RegisterForm from "@/pages/user/authentication/RegisterForm.tsx";

const Register = () => (
  <div className="main-wrap">
    <div className="nav-header bg-transparent shadow-none border-0">
      <div className="nav-top w-100">
        <Link to={'/'}>
          <i className="feather-zap text-success display1-size me-2 ms-0"/>
          <span className="d-inline-block fredoka-font ls-3 fw-600 text-current font-xxl logo-text mb-0">
            {import.meta.env.VITE_NAME}
          </span>
        </Link>
        <Link
          to={'/auth/login'}
          className="header-btn d-none d-lg-block bg-current fw-500 text-white font-xsss p-3 ms-auto w100 text-center lh-20 rounded-xl"
        >
          Login
        </Link>
      </div>
    </div>

    <div className="row">
      <div
        className="col-xl-5 d-none d-xl-block p-0 vh-100 bg-image-cover bg-no-repeat"
        style={{backgroundImage: "url(/assets/images/login-bg-2.jpg)"}}
      />
      <div className="col-xl-7 vh-100 align-items-center d-flex bg-white rounded-3 overflow-hidden">
        <div className="card shadow-none border-0 ms-auto me-auto login-card">
          <div className="card-body rounded-0 text-left">
            <h2 className="fw-700 display1-size display2-md-size mb-4">
              Create <br/>
              your account
            </h2>
            <RegisterForm/>
            <div className="col-sm-12 p-0 text-left">
              <h6 className="text-grey-500 font-xsss fw-500 mt-0 mb-0 lh-32">
                Already have account{" "}
                <Link to={'/auth/login'} className="fw-700 ms-1">
                  Login
                </Link>
              </h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Register;
