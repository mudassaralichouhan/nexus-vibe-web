import {Link} from 'react-router-dom';
import React from "react";
import LoginForm from "@/pages/user/authentication/LoginForm.tsx";

const Login: React.FC = () => (
  <div className="main-wrap">
    <div className="nav-header bg-transparent shadow-none border-0">
      <div className="nav-top w-100">
        <Link to={'/'}>
          <i className="feather-zap text-success display1-size me-2 ms-0"/>
          <span className="d-inline-block fredoka-font ls-3 fw-600 text-current font-xxl logo-text mb-0">
            {import.meta.env.VITE_NAME}
          </span>
        </Link>
        <Link
          to={'/auth/register'}
          className="header-btn d-none d-lg-block bg-current fw-500 text-white font-xsss p-3 ms-auto w100 text-center lh-20 rounded-xl"
        >
          Register
        </Link>
      </div>
    </div>
    <div className="row">
      <div
        className="col-xl-5 d-none d-xl-block p-0 vh-100 bg-image-cover bg-no-repeat"
        style={{backgroundImage: "url(/assets/images/login-bg.jpg)"}}
      />
      <div className="col-xl-7 vh-100 align-items-center d-flex bg-white rounded-3 overflow-hidden">
        <div className="card shadow-none border-0 ms-auto me-auto login-card">
          <div className="card-body rounded-0 text-left">

            <LoginForm/>

            <div className="col-sm-12 p-0 text-left">
              <h6 className="text-grey-500 font-xsss fw-500 mt-0 mb-0 lh-32">
                Dont have account
                <Link to={'/auth/register'} className="fw-700 ms-1">
                  Register
                </Link>
              </h6>
            </div>
            <div className="col-sm-12 p-0 text-center mt-2">
              <h6 className="mb-0 d-inline-block bg-white fw-500 font-xsss text-grey-500 mb-3">
                Or, Sign in with your social account
              </h6>
              <div className="form-group mb-1">
                <a
                  href="#"
                  className="form-control text-left style2-input text-white fw-600 bg-facebook border-0 p-0 mb-2"
                >
                  <img
                    src="/assets/images/icon-1.png"
                    alt="icon"
                    className="ms-2 w40 mb-1 me-5"
                  />
                  Sign in with Google
                </a>
              </div>
              <div className="form-group mb-1">
                <a
                  href="#"
                  className="form-control text-left style2-input text-white fw-600 bg-twiiter border-0 p-0 "
                >
                  <img
                    src="/assets/images/icon-3.png"
                    alt="icon"
                    className="ms-2 w40 mb-1 me-5"
                  />
                  Sign in with Facebook
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Login;
