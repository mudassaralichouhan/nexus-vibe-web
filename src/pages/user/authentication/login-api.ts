import {api, ResponseType} from "@/api";
import {AxiosResponse} from "axios";
import authService from "@/services/auth-service.ts";

export interface LoginType {
  email: string
  password: string
}

export const loginApi = async (formData: LoginType): Promise<ResponseType> => {
  try {
    const response: AxiosResponse = await api.post('/auth/login', formData);

    authService.set({
      isAuthenticated: !!response.data?.token,
      token: response.data?.token,
      expireAt: 0,
      data: {
        ...response.data.user,
        id: response.data?.user?.id,
        name: response.data?.user?.name,
        email: response.data?.user?.email
      }
    });

    return {
      data: response.data,
      error: false,
    };

  } catch (error) {
    return {
      data: error?.response?.data || error.message,
      error: true,
    };
  }
}