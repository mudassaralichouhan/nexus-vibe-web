import React from "react";

import './index.css';
import {Link} from "react-router-dom";

const Home: React.FC = () => {

  return (
    <>
      <div className="header-wrapper">
        <div className="container max-container">
          <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-3 col-xs-6">
              <a href="index.html" className="logo">
                <i className="feather-zap text-success display2-size me-3 ms-0"/>
                <span className="d-inline-block fredoka-font ls-3 fw-600 text-current font-xxl logo-text mb-0">
                  Sociala.
                </span>
              </a>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 d-none d-lg-block">
              <ul className="list-inline text-center mb-0 mt-2 pt-1">
                <li className="list-inline-item pl-4 pr-4">
                  <a className="scroll-tiger" href="#feature">
                    Features
                  </a>
                </li>
                <li className="list-inline-item pl-4 pr-4">
                  <a className="scroll-tiger" href="#demo">
                    Demo
                  </a>
                </li>
                <li className="list-inline-item pl-4 pr-4">
                  <a className="scroll-tiger" href="#contact">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-3 col-xs-6 text-right">
              <Link to={'/auth/login'} className="btn btn-lg btn-primary text-uppercase">
                Join us
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div style={{backgroundImage: "url(/assets/demo/banner-bg-1.png)"}}>
        <div className="container max-container">
          <div className="row">
            <div className="col-lg-5 col-md-6 col-sm-8">
              <h2 className="title-text mb-5 mt-5">
                <b>
                  Set up your <span>Social</span> website with Sociala.
                </b>
              </h2>
              <div className="clearfix"/>
              <a
                href="#demo"
                className="btn btn-lg btn-primary mr-4 text-uppercase mt-5"
              >
                See DEMOs
              </a>
              <div className="icon-scroll pos-bottom-center icon-white"/>
            </div>
          </div>
        </div>
      </div>

      <div className="p100 bg-black">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 text-center">
              <h2 className="title-text2 text-white mt-4">
                <b>Beautiful designs to get you started</b>
              </h2>
              <p className="text-white ml-5 mr-5">
                Create a really awesome website, choose the version that will suit
                your requirements in a best way.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
