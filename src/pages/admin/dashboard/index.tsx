import React, {useEffect, useState} from 'react';
import toTime from 'to-time';
import bytes from 'bytes';
import {dashboardApi} from '@/pages/admin/dashboard/dashboard-api.ts';
import {useNavigate} from "react-router-dom";

const Dashboard: React.FC = () => {
  const navigate = useNavigate();

  const [data, setData] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const result = await dashboardApi();
      console.log(result)
      setData(result.data);

      if (result.error) {
        navigate('/admin/login');
      }
    };

    fetchData();
  }, []);

  return (
    <>
      {toTime('1 hour').seconds()}
      <br/>
      {bytes(1024)}

      {data && (
        <div>
          <h2>JSON Object:</h2>
          <pre>{JSON.stringify(data, null, 2)}</pre>
        </div>
      )}
    </>
  );
};

export default Dashboard;
