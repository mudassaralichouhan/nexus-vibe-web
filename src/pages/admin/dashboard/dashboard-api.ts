import apiClient from "@/api/admin.ts";
import {AxiosResponse} from "axios";
import {ResponseType} from "@/api";

export interface RegisterType {
  email: string
  password: string
}

export const dashboardApi = async (): Promise<ResponseType> => {
  try {
    const response: AxiosResponse = await apiClient.get('/auth');

    return {
      data: response.data,
      error: false,
    };

  } catch (error) {
    return {
      data: error?.response?.data || error.message,
      error: true,
    };
  }
}