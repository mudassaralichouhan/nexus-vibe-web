import {api} from "@/api/admin.ts";
import {AxiosResponse} from "axios";
import authService from "@/services/auth-service.ts";
import {message} from "antd";

export interface LoginType {
  email: string
  password: string
}

export const loginApi = async (formData: LoginType): Promise<ResponseType> => {
  try {
    const response: AxiosResponse = await api.post('/auth/login', formData);

    authService.set({
      isAuthenticated: !!response.data?.accessToken,
      token: response.data?.accessToken,
      refresh_token: response.data?.refreshToken,
      expireAt: 0,
      data: {
        ...response.data.user,
        id: response.data?.user?.id,
        name: response.data?.user?.name,
        email: response.data?.user?.email,
        role: response.data?.user?.role,
      }
    });

    message.success('login successfully!');

    return {
      data: response.data,
      error: false,
    };

  } catch (error) {
    message.error('login fail!');

    return {
      data: error?.response?.data || error.message,
      error: true,
    };
  }
}