import React from "react";

import {Button, Checkbox, Form, Input, message, notification, Typography} from "antd";

import {LockOutlined, MailOutlined} from "@ant-design/icons";
import {Link, useNavigate} from "react-router-dom";
import {NotificationPlacement} from "antd/es/notification/interface";
import {registerApi, RegisterType} from "@/pages/admin/authentication/register/register-api.ts";
import {AuthWrapper, useStyleProps} from "@/layout/AdminLayout/AuthLayout.tsx";

const {Text} = Typography;

const Register: React.FC = () => {

  const [api, contextHolder] = notification.useNotification();
  const navigate = useNavigate();

  const styles = useStyleProps().styles;

  const onFinish = async (form: RegisterType) => {

    const result: ResponseType = await registerApi(form);

    if (!result.error) {
      message.success('Verification code sent on your mail!')
      navigate('/admin/login', {
        state: form,
      });
    } else {

      if (Array.isArray(result.data)) {
        result.data.forEach((entry) => {
          openNotification('bottomRight', entry.context.key, entry.message)
        });
      } else {
        openNotification('bottomRight', result.data.error, "")
      }
    }
  };

  const openNotification = (placement: NotificationPlacement, title: string, message: string) => {
    api.error({
      message: `Warning: ${title}`,
      description: message,
      placement,
    });
  };

  return (
    <AuthWrapper header={'Register'} description={'Specific use case forms like band registration forms'}>
      <Form
        initialValues={{
          remember: false,
        }}
        onFinish={onFinish}
        layout="vertical"
        requiredMark="optional"
      >
        <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: "Please input your Name!",
            }
          ]}
        >
          <Input
            prefix={<MailOutlined/>}
            placeholder="Name"
          />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Please input your Email!",
            }
          ]}
        >
          <Input
            prefix={<MailOutlined/>}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined/>}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item
          name="repeat_password"
          rules={[
            {
              required: true,
              message: "Please input your Repeat Password!",
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined/>}
            type="password"
            placeholder="Repeat Password"
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="terms" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
          <Link style={styles.forgotPassword} to={"/admin/auth/password/forgot"}>
            Forgot password?
          </Link>
        </Form.Item>
        <Form.Item style={{marginBottom: "0px"}}>
          <Button block="true" type="primary" htmlType="submit">
            Log in
          </Button>
          <div style={styles.footer}>
            <Text style={styles.text}>Don't have an account?</Text>{" "}
            <Link to={"/admin/login"}>Sign in now</Link>
          </div>
        </Form.Item>
      </Form>
      {contextHolder}
    </AuthWrapper>
  );
};

export default Register;
