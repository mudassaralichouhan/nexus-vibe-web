import { Grid } from '@mui/material';

const Error404 = () => (
  <Grid container spacing={3}>
    <Grid item xs={12}>
      <h1>Error 404</h1>
    </Grid>
  </Grid>
);

export default Error404;
