import React, {lazy} from 'react';
import {Route, Routes as ReactRoutes} from 'react-router-dom';


import Loadable from "@/layout/SocialLayout/components/SocialLoadable.tsx";
import authService, {AuthType} from "@/services/auth-service.ts";

const SocialLayout = Loadable(lazy(() => import('@/layout/SocialLayout')));

const Index = Loadable(lazy(() => import('@/pages/index')));
const Home = Loadable(lazy(() => import('@/pages/user/home')));
const Error404 = Loadable(lazy(() => import('@/pages/error/404')));

const AuthContent: React.FC = () => {
  const auth: AuthType = authService.get()
  return auth.isAuthenticated ? <Home/> : <Index/>;
};

const publicRoutes = [
  {path: '/', element: <AuthContent/>},
  {path: '404', element: <Error404/>},
];

const PublicRoutes: React.FC = () => {
  return (
    <ReactRoutes>
      <Route element={<SocialLayout/>}>
        {publicRoutes.map(route => (
          <Route key={route.path} path={route.path} element={route.element}/>
        ))}
      </Route>
    </ReactRoutes>
  );
}

export default PublicRoutes;
