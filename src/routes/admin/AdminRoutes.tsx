import React, {lazy} from "react";
import {Route, Routes as ReactRoutes} from 'react-router-dom'

import Loadable from "@/layout/AdminLayout/components/Loadable.tsx";
import {DashOutlined} from "@ant-design/icons";

const Dashboard = Loadable(lazy(() => import("@/pages/admin/dashboard")));
const Login = Loadable(lazy(() => import("@/pages/admin/authentication/login/Login.tsx")));
const Register = Loadable(lazy(() => import("@/pages/admin/authentication/register/Register.tsx")));
const OTP = Loadable(lazy(() => import("@/pages/admin/authentication/login/OTP.tsx")));

const AdminLayout = Loadable(lazy(() => import('@/layout/AdminLayout')));
const AuthLayout = Loadable(lazy(() => import('@/layout/AdminLayout/AuthLayout.tsx')));

export const ADMIN_ROUTE_PREFIX = 'admin';

export const adminRoutes = [
  {path: 'dashboard', element: <Dashboard/>, name: 'dashboard', icon: <DashOutlined/>},
];

const adminAuthRoutes = [
  {path: 'login', element: <Login/>, name: 'login'},
  {path: 'register', element: <Register/>, name: 'register'},
  {path: 'otp', element: <OTP/>, name: 'otp'},
];

const AdminRoutes: React.FC = () => {
  return (
    <ReactRoutes>
      <Route path={ADMIN_ROUTE_PREFIX} element={<AdminLayout/>}>
        {adminRoutes.map(route => (
          <Route key={route.path} path={route.path} element={route.element}/>
        ))}
      </Route>
      <Route path={ADMIN_ROUTE_PREFIX} element={<AuthLayout/>}>
        {adminAuthRoutes.map(route => (
          <Route key={route.path} path={route.path} element={route.element}/>
        ))}
      </Route>
    </ReactRoutes>
  );
}
export default AdminRoutes;
