import React from "react";

import AdminRoutes from "@/routes/admin/AdminRoutes.tsx";
import PublicRoutes from "@/routes/PublicRoutes.tsx";
import AuthRoutes from "@/routes/user/AuthRoutes.tsx";
import UserRoutes from "@/routes/user/UserRoutes.tsx";
import authService, {AuthType} from "@/services/auth-service.ts";

const Routes: React.FC = () => {

  const auth: AuthType = authService.get()

  return (
    <>
      <PublicRoutes/>
      {!auth.isAuthenticated && <AuthRoutes/>}
      <UserRoutes/>
      <AdminRoutes/>
    </>
  );
}

export default Routes;
