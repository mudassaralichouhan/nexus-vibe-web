import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

export const humanReadable = (timestamp: string) => {
  dayjs.extend(relativeTime);
  return dayjs(timestamp).fromNow();
}