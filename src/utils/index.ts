export const setPageTitle = (title: string) => {
  window.document.title = title;
};