import { Suspense, ComponentType } from 'react';
import SocialLoader from "@/layout/SocialLayout/components/SocialLoader.tsx";

const SocialLoadable = <P extends object>(Component: ComponentType<P>) => (props: P) => (
  <Suspense fallback={<SocialLoader />}>
    <Component {...props} />
  </Suspense>
);

export default SocialLoadable;
