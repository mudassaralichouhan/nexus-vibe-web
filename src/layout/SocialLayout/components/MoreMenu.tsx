import React from "react";
import {Link} from "react-router-dom";
import {Menu, MenuItem} from "@mui/material";
import {Bookmark as BookmarkIcon} from "@mui/icons-material";

export const MoreMenu = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  
  return (
    <React.Fragment>
      <Link
        to={''}
        aria-haspopup="true"
        onClick={handleMenuClick}
      >
        <i className="ti-more-alt text-grey-900 btn-round-md bg-greylight font-xss"/>
      </Link>
      
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <MenuItem>
          <BookmarkIcon className="text-grey-500 me-3 font-lg"/>
          <div>
            <h4 className="fw-600 text-grey-900 font-xssss mt-0 me-4">Save Link</h4>
            <span className="d-block font-xsssss fw-500 mt-1 lh-3 text-grey-500">Add this to your saved items</span>
          </div>
        </MenuItem>
        <MenuItem>
          <i className="feather-alert-circle text-grey-500 me-3 font-lg"/>
          <div>
            <h4 className="fw-600 text-grey-900 font-xssss mt-0 me-4">Hide Post</h4>
            <span className="d-block font-xsssss fw-500 mt-1 lh-3 text-grey-500">Save to your saved items</span>
          </div>
        </MenuItem>
        <MenuItem>
          <i className="feather-alert-octagon text-grey-500 me-3 font-lg"/>
          <div>
            <h4 className="fw-600 text-grey-900 font-xssss mt-0 me-4">Hide all from Group</h4>
            <span className="d-block font-xsssss fw-500 mt-1 lh-3 text-grey-500">Save to your saved items</span>
          </div>
        </MenuItem>
        <MenuItem>
          <i className="feather-lock text-grey-500 me-3 font-lg"/>
          <div>
            <h4 className="fw-600 mb-0 text-grey-900 font-xssss mt-0 me-4">Unfollow Group</h4>
            <span className="d-block font-xsssss fw-500 mt-1 lh-3 text-grey-500">Save to your saved items</span>
          </div>
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
};