import React, {useEffect} from 'react';
import {Outlet} from 'react-router-dom';

import '@/layout/SocialLayout/assets/css/feather.css';
import '@/layout/SocialLayout/assets/css/style.css';
import '@/layout/SocialLayout/assets/css/lightbox.css';
import '@/layout/SocialLayout/assets/css/video-player.css';
import '@/layout/SocialLayout/assets/css/themify-icons.css';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";

const SocialLayout: React.FC = () => {
  useEffect(() => {
    document.body.className = 'color-theme-blue mont-font';
  }, []);

  return (
    <>
      <Outlet />
      <ToastContainer
        position="top-right"
        autoClose={5 * 1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick={false}
        pauseOnFocusLoss
        draggable={true}
        pauseOnHover={true}
        theme="light"
      />
    </>
  );
};

export default SocialLayout;
