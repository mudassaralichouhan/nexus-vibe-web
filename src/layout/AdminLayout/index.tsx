import React, {memo} from 'react';
import {Link, Outlet, useLocation} from "react-router-dom";

import {Dropdown} from 'antd';
import {ProLayout} from '@ant-design/pro-components';

import {LogoutOutlined} from "@mui/icons-material";
import {UsergroupAddOutlined} from "@ant-design/icons";
import {ADMIN_ROUTE_PREFIX, adminRoutes} from "@/routes/admin/AdminRoutes.tsx";

const AdminLayout: React.FC = () => {
  const location = useLocation();

  const defaultProps = {
    route: {
      routes: adminRoutes,
    },
  };

  return (
    <div className="h-screen">
      <ProLayout
        title={'Nexus Vibe'}
        logo={'/icon.jpg'}
        token={{
          sider: {
            colorMenuBackground: 'white',
          },
        }}
        fixedHeader={true}
        fixSiderbar={true}
        layout={'mix'}
        location={location}
        route={defaultProps.route}
        menuItemRender={(menuItemProps, defaultDom) => {
          if (menuItemProps.isUrl || menuItemProps.children) {
            return defaultDom;
          }

          if (menuItemProps.path && location.pathname !== menuItemProps.path) {
            return (
              <Link to={`/${ADMIN_ROUTE_PREFIX}${menuItemProps.path}`} target={menuItemProps.target}>
                {defaultDom}
              </Link>
            );
          }
          return defaultDom;
        }}
        avatarProps={{
          icon: <UsergroupAddOutlined/>,
          className: 'bg-primary bg-opacity-20 text-primary text-opacity-90',
          size: 'small',
          shape: 'square',
          title: 'Admin Name ...',
          render: (_, dom) => {
            return (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: 'logout',
                      icon: <LogoutOutlined/>,
                      label: 'Logout',
                      onClick: () => {

                      },
                    },
                  ],
                }}
              >
                {dom}
              </Dropdown>
            );
          },
        }}
      >
        <Outlet/>
      </ProLayout>
    </div>
  )
    ;
};

export default memo(AdminLayout);