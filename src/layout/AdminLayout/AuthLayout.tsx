import React, {createContext, ReactNode} from "react";
import {Outlet} from 'react-router';

import {Col, Grid, Row, theme, Typography} from 'antd';
import {ProConfigProvider} from "@ant-design/pro-components";
import {css, Global} from '@emotion/react';

const {useToken} = theme;
const {useBreakpoint} = Grid;
const {Text, Title} = Typography;

const StylePropsContext = createContext({});

export const useStyleProps = () => {
  const {token} = useToken();
  const screens = useBreakpoint();

  return {
    styles: {
      container: {
        margin: "0 auto",
        padding: screens.md
          ? `${token.paddingXL}px`
          : `${token.sizeXXL}px ${token.padding}px`,
        width: "380px",
      },
      footer: {
        marginTop: token.marginLG,
        textAlign: "center",
        width: "100%",
      },
      forgotPassword: {
        float: "right",
      },
      header: {
        marginBottom: token.marginXL,
      },
      section: {
        alignItems: "center",
        backgroundColor: token.colorBgContainer,
        display: "flex",
        height: screens.sm ? "100vh" : "auto",
        padding: screens.md ? `${token.sizeXXL}px 0px` : "0px",
      },
      text: {
        color: token.colorTextSecondary,
      },
      title: {
        fontSize: screens.md ? token.fontSizeHeading2 : token.fontSizeHeading3,
      },
    },
  };
};

interface AuthWrapperProps {
  children: ReactNode;
  header?: string;
  description?: string;
}

export const AuthWrapper: React.FC<AuthWrapperProps> = ({children, header, description}) => {

  const styles = useStyleProps().styles;

  return (
    <StylePropsContext.Provider value={styles}>
      <GlobalStyles/>
      <Row>
        <Col span={24}>
          <section style={styles.section}>
            <div style={styles.container}>
              <div style={styles.header}>
                <img src={'/public/icon.jpg'} alt={'logo'}/>
                <Title style={styles.title}>{header}</Title>
                <Text style={styles.text}>
                  {description}
                </Text>
              </div>
              {children}
            </div>
          </section>
        </Col>
      </Row>
    </StylePropsContext.Provider>
  );
};

function GlobalStyles() {
  return (
    <Global
      styles={css`
        body {
          padding: 0;
          margin: 0;
        }
      `}
    />
  );
}

const AuthLayout: React.FC = () => {
  return (
    <ProConfigProvider dark>
      <Outlet/>
    </ProConfigProvider>
  );
};

export default AuthLayout;
