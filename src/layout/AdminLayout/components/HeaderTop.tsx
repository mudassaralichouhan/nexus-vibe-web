import React from 'react';

import {Layout, theme} from 'antd';

const {Header} = Layout;

const HeaderTop: React.FC = () => {
  const {
    token: {colorBgContainer}
  } = theme.useToken();

  return (
    <Header style={{padding: 0, background: colorBgContainer}}>
      <h1>header</h1>
    </Header>
  );
};

export default HeaderTop;