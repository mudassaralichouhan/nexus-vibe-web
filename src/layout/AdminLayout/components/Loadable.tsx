import {ComponentType, Suspense} from 'react';
import {Spin} from 'antd';

const Loadable = <P extends object>(Component: ComponentType<P>) => (props: P) => (
  <Suspense fallback={<Spin size="large" />}>
    <Component {...props} />
  </Suspense>
);

export default Loadable;
