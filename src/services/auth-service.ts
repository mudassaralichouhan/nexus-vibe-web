import {decryptData, encryptData} from "@/helpers/encryption.ts";

interface UserType {
  id: number
  name: string
  email: string
  role: string
}

export interface AuthType {
  isAuthenticated: boolean
  token: string | null
  refresh_token: string | null
  expireAt: number
  data: UserType | null
}

const authService = {
  set: (data: AuthType) => {
    localStorage.setItem('auth', encryptData(data));
  },
  get: (): AuthType => {
    const storage = decryptData(localStorage.getItem('auth') || '');

    return {
      isAuthenticated: storage?.isAuthenticated === true,
      token: storage?.token,
      refresh_token: storage?.refresh_token,
      expireAt: storage?.expireAt,
      data: storage?.data,
    }
  }
}

export default authService;